module jdbc_example {
	exports jdbc;

	requires java.sql;
	// requires that postgresql jdbc driver is on the MODULE PATH, not class path
	requires org.postgresql.jdbc;
}