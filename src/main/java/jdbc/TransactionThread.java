package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class TransactionThread implements Runnable {
  
  /**
   * We might want to have multiply connections to the DB
   * at the same time.
   */
  private Connection db = null;
  
  /**
   * Establishes a connection with the database.
   * 
   * @param port the port used for the database connection
   * @param username your user name for the database connection
   * @param password your password for the database connection
   * @param database the database to connect to
   * @throws SQLException if a database access error occurs
   */
  public TransactionThread(String port, String username, String password, String database) throws SQLException {
    this.db = DriverManager.getConnection("jdbc:postgresql://192.168.122.26:"
        + port + "/" + database, username, password);
    if (this.db == null) {
      System.err.println("ERROR: could not connect to Data Base");
    }
    this.db.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
    this.db.setAutoCommit(false);
    this.db.commit();
  }
  
  /**
   * return the balance of the account with the accountid
   * @param accid account id
   * @return the balance of the account that is linked with the accountid
   * @throws SQLException when SQL fails to execute the query
   */
  public int balance(int accid) throws SQLException{
    PreparedStatement ps = this.db.prepareStatement("SELECT balance FROM accounts WHERE accid = ?;");
    ps.setInt(1, accid);
    ResultSet rs = ps.executeQuery();
    rs.next();
    return rs.getInt(1);
  }

  /**
   * adds the delta to the balance of the account with accID
   * @param accId accountId of the account
   * @param tellerId Id of the Teller
   * @param branchId Id of the branch
   * @param delta the amount by that the balance should be changed
   * @return the new balance of the account
   * @throws SQLException when sql fails to execute query
   */
  public int deposit(int accId, int tellerId, int branchId, int delta) throws SQLException {
    // TODO: accounts -> tellers -> balance to prevent dead log -> pls check before implementing
    int newBalance = this.balance(accId) + delta;

    // TODO: remove prepared statements from here
    // Update balance in branches
    PreparedStatement psBranches = this.db.prepareStatement("UPDATE branches SET balance = ? WHERE branchid=? ;");
    psBranches.setInt(1, newBalance);
    psBranches.setInt(2, branchId);

    //Update balance in terllerId
    PreparedStatement psTeller = this.db.prepareStatement("UPDATE tellers SET balance = ? WHERE tellerId=?;");
    psTeller.setInt(1, newBalance);
    psTeller.setInt(2, tellerId);

    //Update balance in accounts
    PreparedStatement psAccs = this.db.prepareStatement("UPDATE accounts SET balance=? WHERE accid=?;");
    psAccs.setInt(1, newBalance);
    psAccs.setInt(2, accId);

    //Insert to history
    PreparedStatement psHistory = this.db.prepareStatement("INSERT INTO history VALUES(?, ?, ? ,? ,?,?);");
    psHistory.setInt(1,accId);
    psHistory.setInt(2, tellerId);
    psHistory.setInt(3, delta);
    psHistory.setInt(4, branchId);
    psHistory.setInt(5, newBalance);
    psHistory.setString(6,"" ); // empty string because i dont know what cmmnt means in history

    //execute prepared Statements
    psBranches.executeUpdate();
    psTeller.executeUpdate();
    psAccs.executeUpdate();
    psHistory.executeUpdate();

    return newBalance;
  }

  /**
   *
   * @param delta
   * @return  the count of tuples in the history relation with the given delta
   * @throws SQLException
   */
  public int analyseTX(int delta) throws SQLException {
    PreparedStatement psAnalyse = this.db.prepareStatement("SELECT COUNT(accid) FROM history where delta=?;");
    psAnalyse.setInt(1, delta);
    
    ResultSet rs = psAnalyse.executeQuery();
    rs.next();
    return rs.getInt(1);
  }
  
  /**
   * deletes everything inside the history relation
   * @throws SQLException
   */
  private  void deleteHistory() throws SQLException{
    Statement stm = this.db.createStatement();
    String query = "DELETE FROM history";
    stm.execute(query);
  }
  
  /**
   * Executes a benchmark.
   * 
   * @throws InterruptedException
   * @throws SQLException
   */
  public void executeBenchmark() throws InterruptedException, SQLException {
    Random rand = new Random(); 
    
    final long start = System.nanoTime();
    long end = System.nanoTime();
    long counter = 0;
    
    System.out.println(Thread.currentThread().getName() + ": Benchmark started");
    // while end - start is less than 600s
    while ((end - start) / 1000000000 < 600) {
      // TODO: we will crash here due to postgresql not having a 2-phase scheduler
      // and auto-commit turned off
      if((end - start) / 1000000000 > 240 && (end - start) / 1000000000 < 540) {
        counter++;
      }
      int randDelta = rand.nextInt(10000)+1;
      int transact = rand.nextInt(100) + 1 ;
      int randbranch = rand.nextInt(100)+1;
      int randaccId = rand.nextInt(10000000)+1;
      int randTeller = rand.nextInt(1000) + 1;
      this.deleteHistory();
      
      if (transact > 0 && transact <= 15) {
        this.analyseTX(randDelta);
      } else if (transact > 15 && transact <= 50) {
        this.balance(randaccId);
      } else if (transact > 50 && transact <= 100) {
        this.deposit(randaccId,randTeller,randbranch,randDelta);
      }
      
      Thread.sleep(50); 
      end = System.nanoTime();
    }
    System.out.println(Thread.currentThread().getName() + ": Transactions: " + counter);
  }
  @Override
  public void run() {
    try {
      this.executeBenchmark();
    } catch (InterruptedException | SQLException e) {
      e.printStackTrace();
    }
  }
}
