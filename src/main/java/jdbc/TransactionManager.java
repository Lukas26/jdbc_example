package jdbc;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class TransactionManager {
  
  /**
   * A scanner from the System.in input stream is only
   * allowed to exist once per application.
   */
  public static Scanner sc = new Scanner(System.in);
  
  /**
   * We might want to have multiply connections to the DB
   * at the same time.
   */
  private Connection db = null;
  
  private int numOfThreads = 5;
  private ThreadPoolExecutor executer = null;
  
  /**
   * Creates a new thread pool.
   */
  public TransactionManager() {
    this.executer = (ThreadPoolExecutor) Executors.newFixedThreadPool(this.numOfThreads);
  }
  
  /**
   * Deletes all Data in the Database.
   * 
   * @throws SQLException when SQL fails to execute the query
   */
  private void deleteDB() throws SQLException {
    Statement stm = this.db.createStatement();
    String query = "DELETE FROM tellers";
    stm.execute(query);
    
    query = "DELETE FROM accounts";
    stm.execute(query);
    
    query = "DELETE FROM branches";
    stm.execute(query);
  }

  /**
   * Start the tps benchmark.
   * 
   * @param n n times 100'000 executions for the benchmark
   * @throws SQLException could throw an sql execution
   */
  public void ntps(int n) throws SQLException {
    this.deleteDB();
    System.out.println("Deleted database");
    PreparedStatement preStatBranches = this.db.prepareStatement("INSERT INTO branches VALUES(?,"
        + "'01234567890123456789',0,'Ykw9jdd1dZkw4Zx6mZEN7yn6Oz5rrZ9DXpVDsmwd8e"
        + "NUtsRik6Ew4B90ey2PY9zrKLV6rgFo')");
    PreparedStatement preStatAccounts = this.db.prepareStatement("INSERT INTO accounts VALUES(?,"
        + "'01234567890123456789',0,?,"
        + "'Ykw9jdd1dZkw4Zx6mZEN7yn6Oz5rrZ9DXpVDsmwd8eNUtsRik6Ew4B90ey2PY9zrKLV6')");
    PreparedStatement preStatTellers = this.db.prepareStatement("INSERT INTO tellers VALUES(?,"
        + "'01234567890123456789',0,?,"
        + "'Ykw9jdd1dZkw4Zx6mZEN7yn6Oz5rrZ9DXpVDsmwd8eNUtsRik6Ew4B90ey2PY9zrKLV6')");
    System.out.println("start executing sql queries:");
    final long start = System.currentTimeMillis();
    
    int batchSize = 1000;
    for (int batchIndex = 0; batchIndex <= n / batchSize; ++batchIndex) {
      int maxIndex = (batchIndex + 1) * batchSize;
      
      // we have to set match Index to n if we finished with all full batches
      // but have not finished with all n
      if (n / ((batchIndex + 1) * batchSize) < 1) {
        maxIndex = n;
      }
      for (int i = batchIndex * batchSize; i < maxIndex; ++i) {
        preStatBranches.setInt(1, i + 1);
        preStatBranches.addBatch();
      }
      preStatBranches.executeBatch();
      preStatBranches.clearBatch();
    }
    
    for (int batchIndex = 0; batchIndex <= (n * 100000) / batchSize; ++batchIndex) {
      int maxIndex = (batchIndex + 1) * batchSize;
      
      // we have to set match Index to n if we finished with all full batches
      // but have not finished with all n
      if ((n * 100000) / ((batchIndex + 1) * batchSize) < 1) {
        maxIndex = (n * 100000);
      }
      for (int i = batchIndex * batchSize; i < maxIndex; ++i) {
        preStatAccounts.setInt(1, i + 1);
        int temp = (int) (Math.random() * n) + 1;
        preStatAccounts.setInt(2, temp);
        preStatAccounts.addBatch();
      }
      preStatAccounts.executeBatch();
      preStatAccounts.clearBatch();
    }
    
    for (int batchIndex = 0; batchIndex <= (n * 10) / batchSize; ++batchIndex) {
      int maxIndex = (batchIndex + 1) * batchSize;
      
      // we have to set match Index to n if we finished with all full batches
      // but have not finished with all n
      if ((n * 10) / ((batchIndex + 1) * batchSize) < 1) {
        maxIndex = (n * 10);
      }
      for (int i = batchIndex * batchSize; i < maxIndex; ++i) {
        preStatTellers.setInt(1, i + 1);
        int temp = (int) (Math.random() * n) + 1;
        preStatTellers.setInt(2, temp);
        preStatTellers.addBatch();
      }
      preStatTellers.executeBatch();
      preStatTellers.clearBatch();
    }
    
    final long end = System.currentTimeMillis();

    System.out.println("inserted to DATABASE");
    System.out.println("Duration:" + ((end - start) / 1000) + "s");

    preStatAccounts.close();
    preStatBranches.close();
    preStatTellers.close();
  }
  
  /**
   * Reads in a config file and returns it values in a list.
   * 
   * @param path the path to the config file
   * @return the values in a list
   * @throws IOException if an I/O error occurs reading from the file
   */
  public static List<String> readConfig(String path) throws IOException {
    List<String> loginData = new ArrayList<String>();
    if(Files.exists(Paths.get(path))) {
      loginData = Files.readAllLines(Paths.get("config.txt"));
      return loginData;
    }
    
    return null;
  }
  
  /**
   * Creates a new config file and asks the user for the values to be saved
   * in this new config file. If it already exists, it reads it in.
   * The values will be returned in a list.
   * 
   * @param path the path to the config file
   * @return the values in a list
   * @throws IOException if an I/O error occurs reading from or writing to the file
   */
  
  public static List<String> createConfig(String path) throws IOException {
    List<String> loginData = new ArrayList<String>();
    if(Files.exists(Paths.get(path))) {
      return TransactionManager.readConfig(path);
    }
    
    System.out.println("Please enter your data once to save it in the config file");
    System.out.println("Please enter your port ");
    if (TransactionManager.sc.hasNextLine()) {
      loginData.add(TransactionManager.sc.nextLine());
    }
    
    System.out.println("Please enter your username");

    if (TransactionManager.sc.hasNextLine()) {
      loginData.add(TransactionManager.sc.nextLine());
    }
    // We could hide how the user types in the password, like it is done in most Linux
    // applications, but this does not work with how IntelliJ and Eclipse implement
    // their internal console.
    System.out.println("Please enter your password or press enter to use the default one: ");
    if (TransactionManager.sc.hasNextLine()) {
      loginData.add(TransactionManager.sc.nextLine());
    }

    System.out.println("Please enter your database name:");
    if (TransactionManager.sc.hasNextLine()) {
      loginData.add(TransactionManager.sc.nextLine());
    }

    Files.write(Paths.get("config.txt"), loginData, StandardCharsets.UTF_8);
    
    return loginData;
  }
  
  /**
   * Establishes a connection with the database.
   * 
   * @param port the port used for the database connection
   * @param username your user name for the database connection
   * @param password your password for the database connection
   * @param database the database to connect to
   * @throws SQLException if a database access error occurs
   */
  public void benchmark(String port, String username, String password, String database) throws SQLException {
    for(int i = 0; i < this.numOfThreads ; i++) {
      Runnable worker = new TransactionThread(port, username, password, database);
      this.executer.submit(worker);
    }
    this.executer.shutdown();
    while (!this.executer.isTerminated()) { }
  }

  /**
   * The main method of our program.
   * 
   * @param args arguments for startup, currently unused
   */
  public static void main(String[] args) {
    List<String> loginData = null;
    try {
      loginData = TransactionManager.createConfig("config.txt");
    } catch (IOException e) {
      e.printStackTrace();
    }

    if(loginData != null) {
      TransactionManager client;
      try {
        client = new TransactionManager();
        client.benchmark(loginData.get(0), loginData.get(1), loginData.get(2), loginData.get(3));
        //client.ntps(100); // for 100: 607s with Batchsize 25, 417s with Batchsize 100, 364s with Batchsize 1000
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    
    // this has to be the last line of our application
    // Java will close it automatically, but to be sure :)
    TransactionManager.sc.close();
  }
}
